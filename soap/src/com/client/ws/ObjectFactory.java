
package com.client.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.client.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddPackageResponse_QNAME = new QName("http://ws/", "addPackageResponse");
    private final static QName _GetPackageRoute_QNAME = new QName("http://ws/", "getPackageRoute");
    private final static QName _GetUserByUsernameResponse_QNAME = new QName("http://ws/", "getUserByUsernameResponse");
    private final static QName _SearchPackageByIdResponse_QNAME = new QName("http://ws/", "searchPackageByIdResponse");
    private final static QName _RemovePackageByIdResponse_QNAME = new QName("http://ws/", "removePackageByIdResponse");
    private final static QName _GetUserByUsername_QNAME = new QName("http://ws/", "getUserByUsername");
    private final static QName _GetRouteById_QNAME = new QName("http://ws/", "getRouteById");
    private final static QName _RemovePackageById_QNAME = new QName("http://ws/", "removePackageById");
    private final static QName _SaveNewUserResponse_QNAME = new QName("http://ws/", "saveNewUserResponse");
    private final static QName _AddPackage_QNAME = new QName("http://ws/", "addPackage");
    private final static QName _CheckStatusByPackageId_QNAME = new QName("http://ws/", "checkStatusByPackageId");
    private final static QName _GetPackageRouteResponse_QNAME = new QName("http://ws/", "getPackageRouteResponse");
    private final static QName _SaveNewUser_QNAME = new QName("http://ws/", "saveNewUser");
    private final static QName _RegisterPackageResponse_QNAME = new QName("http://ws/", "registerPackageResponse");
    private final static QName _CheckStatusByPackageIdResponse_QNAME = new QName("http://ws/", "checkStatusByPackageIdResponse");
    private final static QName _GetRouteByIdResponse_QNAME = new QName("http://ws/", "getRouteByIdResponse");
    private final static QName _RegisterPackage_QNAME = new QName("http://ws/", "registerPackage");
    private final static QName _UpdatePackageStatusByIdResponse_QNAME = new QName("http://ws/", "updatePackageStatusByIdResponse");
    private final static QName _UpdatePackageStatusById_QNAME = new QName("http://ws/", "updatePackageStatusById");
    private final static QName _SearchPackageById_QNAME = new QName("http://ws/", "searchPackageById");
    private final static QName _ListAllPackageResponse_QNAME = new QName("http://ws/", "listAllPackageResponse");
    private final static QName _ListAllPackage_QNAME = new QName("http://ws/", "listAllPackage");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.client.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddPackageResponse }
     * 
     */
    public AddPackageResponse createAddPackageResponse() {
        return new AddPackageResponse();
    }

    /**
     * Create an instance of {@link GetPackageRoute }
     * 
     */
    public GetPackageRoute createGetPackageRoute() {
        return new GetPackageRoute();
    }

    /**
     * Create an instance of {@link GetUserByUsernameResponse }
     * 
     */
    public GetUserByUsernameResponse createGetUserByUsernameResponse() {
        return new GetUserByUsernameResponse();
    }

    /**
     * Create an instance of {@link SearchPackageByIdResponse }
     * 
     */
    public SearchPackageByIdResponse createSearchPackageByIdResponse() {
        return new SearchPackageByIdResponse();
    }

    /**
     * Create an instance of {@link RemovePackageByIdResponse }
     * 
     */
    public RemovePackageByIdResponse createRemovePackageByIdResponse() {
        return new RemovePackageByIdResponse();
    }

    /**
     * Create an instance of {@link GetUserByUsername }
     * 
     */
    public GetUserByUsername createGetUserByUsername() {
        return new GetUserByUsername();
    }

    /**
     * Create an instance of {@link GetRouteById }
     * 
     */
    public GetRouteById createGetRouteById() {
        return new GetRouteById();
    }

    /**
     * Create an instance of {@link RemovePackageById }
     * 
     */
    public RemovePackageById createRemovePackageById() {
        return new RemovePackageById();
    }

    /**
     * Create an instance of {@link SaveNewUserResponse }
     * 
     */
    public SaveNewUserResponse createSaveNewUserResponse() {
        return new SaveNewUserResponse();
    }

    /**
     * Create an instance of {@link AddPackage }
     * 
     */
    public AddPackage createAddPackage() {
        return new AddPackage();
    }

    /**
     * Create an instance of {@link CheckStatusByPackageId }
     * 
     */
    public CheckStatusByPackageId createCheckStatusByPackageId() {
        return new CheckStatusByPackageId();
    }

    /**
     * Create an instance of {@link GetPackageRouteResponse }
     * 
     */
    public GetPackageRouteResponse createGetPackageRouteResponse() {
        return new GetPackageRouteResponse();
    }

    /**
     * Create an instance of {@link SaveNewUser }
     * 
     */
    public SaveNewUser createSaveNewUser() {
        return new SaveNewUser();
    }

    /**
     * Create an instance of {@link RegisterPackageResponse }
     * 
     */
    public RegisterPackageResponse createRegisterPackageResponse() {
        return new RegisterPackageResponse();
    }

    /**
     * Create an instance of {@link CheckStatusByPackageIdResponse }
     * 
     */
    public CheckStatusByPackageIdResponse createCheckStatusByPackageIdResponse() {
        return new CheckStatusByPackageIdResponse();
    }

    /**
     * Create an instance of {@link GetRouteByIdResponse }
     * 
     */
    public GetRouteByIdResponse createGetRouteByIdResponse() {
        return new GetRouteByIdResponse();
    }

    /**
     * Create an instance of {@link RegisterPackage }
     * 
     */
    public RegisterPackage createRegisterPackage() {
        return new RegisterPackage();
    }

    /**
     * Create an instance of {@link UpdatePackageStatusByIdResponse }
     * 
     */
    public UpdatePackageStatusByIdResponse createUpdatePackageStatusByIdResponse() {
        return new UpdatePackageStatusByIdResponse();
    }

    /**
     * Create an instance of {@link UpdatePackageStatusById }
     * 
     */
    public UpdatePackageStatusById createUpdatePackageStatusById() {
        return new UpdatePackageStatusById();
    }

    /**
     * Create an instance of {@link SearchPackageById }
     * 
     */
    public SearchPackageById createSearchPackageById() {
        return new SearchPackageById();
    }

    /**
     * Create an instance of {@link ListAllPackageResponse }
     * 
     */
    public ListAllPackageResponse createListAllPackageResponse() {
        return new ListAllPackageResponse();
    }

    /**
     * Create an instance of {@link ListAllPackage }
     * 
     */
    public ListAllPackage createListAllPackage() {
        return new ListAllPackage();
    }

    /**
     * Create an instance of {@link Date }
     * 
     */
    public Date createDate() {
        return new Date();
    }

    /**
     * Create an instance of {@link RouteEntity }
     * 
     */
    public RouteEntity createRouteEntity() {
        return new RouteEntity();
    }

    /**
     * Create an instance of {@link UserEntity }
     * 
     */
    public UserEntity createUserEntity() {
        return new UserEntity();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link PackageEntity }
     * 
     */
    public PackageEntity createPackageEntity() {
        return new PackageEntity();
    }

    /**
     * Create an instance of {@link Route }
     * 
     */
    public Route createRoute() {
        return new Route();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "addPackageResponse")
    public JAXBElement<AddPackageResponse> createAddPackageResponse(AddPackageResponse value) {
        return new JAXBElement<AddPackageResponse>(_AddPackageResponse_QNAME, AddPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPackageRoute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "getPackageRoute")
    public JAXBElement<GetPackageRoute> createGetPackageRoute(GetPackageRoute value) {
        return new JAXBElement<GetPackageRoute>(_GetPackageRoute_QNAME, GetPackageRoute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByUsernameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "getUserByUsernameResponse")
    public JAXBElement<GetUserByUsernameResponse> createGetUserByUsernameResponse(GetUserByUsernameResponse value) {
        return new JAXBElement<GetUserByUsernameResponse>(_GetUserByUsernameResponse_QNAME, GetUserByUsernameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchPackageByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "searchPackageByIdResponse")
    public JAXBElement<SearchPackageByIdResponse> createSearchPackageByIdResponse(SearchPackageByIdResponse value) {
        return new JAXBElement<SearchPackageByIdResponse>(_SearchPackageByIdResponse_QNAME, SearchPackageByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemovePackageByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "removePackageByIdResponse")
    public JAXBElement<RemovePackageByIdResponse> createRemovePackageByIdResponse(RemovePackageByIdResponse value) {
        return new JAXBElement<RemovePackageByIdResponse>(_RemovePackageByIdResponse_QNAME, RemovePackageByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByUsername }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "getUserByUsername")
    public JAXBElement<GetUserByUsername> createGetUserByUsername(GetUserByUsername value) {
        return new JAXBElement<GetUserByUsername>(_GetUserByUsername_QNAME, GetUserByUsername.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRouteById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "getRouteById")
    public JAXBElement<GetRouteById> createGetRouteById(GetRouteById value) {
        return new JAXBElement<GetRouteById>(_GetRouteById_QNAME, GetRouteById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemovePackageById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "removePackageById")
    public JAXBElement<RemovePackageById> createRemovePackageById(RemovePackageById value) {
        return new JAXBElement<RemovePackageById>(_RemovePackageById_QNAME, RemovePackageById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveNewUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "saveNewUserResponse")
    public JAXBElement<SaveNewUserResponse> createSaveNewUserResponse(SaveNewUserResponse value) {
        return new JAXBElement<SaveNewUserResponse>(_SaveNewUserResponse_QNAME, SaveNewUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "addPackage")
    public JAXBElement<AddPackage> createAddPackage(AddPackage value) {
        return new JAXBElement<AddPackage>(_AddPackage_QNAME, AddPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckStatusByPackageId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "checkStatusByPackageId")
    public JAXBElement<CheckStatusByPackageId> createCheckStatusByPackageId(CheckStatusByPackageId value) {
        return new JAXBElement<CheckStatusByPackageId>(_CheckStatusByPackageId_QNAME, CheckStatusByPackageId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPackageRouteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "getPackageRouteResponse")
    public JAXBElement<GetPackageRouteResponse> createGetPackageRouteResponse(GetPackageRouteResponse value) {
        return new JAXBElement<GetPackageRouteResponse>(_GetPackageRouteResponse_QNAME, GetPackageRouteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveNewUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "saveNewUser")
    public JAXBElement<SaveNewUser> createSaveNewUser(SaveNewUser value) {
        return new JAXBElement<SaveNewUser>(_SaveNewUser_QNAME, SaveNewUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "registerPackageResponse")
    public JAXBElement<RegisterPackageResponse> createRegisterPackageResponse(RegisterPackageResponse value) {
        return new JAXBElement<RegisterPackageResponse>(_RegisterPackageResponse_QNAME, RegisterPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckStatusByPackageIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "checkStatusByPackageIdResponse")
    public JAXBElement<CheckStatusByPackageIdResponse> createCheckStatusByPackageIdResponse(CheckStatusByPackageIdResponse value) {
        return new JAXBElement<CheckStatusByPackageIdResponse>(_CheckStatusByPackageIdResponse_QNAME, CheckStatusByPackageIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRouteByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "getRouteByIdResponse")
    public JAXBElement<GetRouteByIdResponse> createGetRouteByIdResponse(GetRouteByIdResponse value) {
        return new JAXBElement<GetRouteByIdResponse>(_GetRouteByIdResponse_QNAME, GetRouteByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "registerPackage")
    public JAXBElement<RegisterPackage> createRegisterPackage(RegisterPackage value) {
        return new JAXBElement<RegisterPackage>(_RegisterPackage_QNAME, RegisterPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePackageStatusByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "updatePackageStatusByIdResponse")
    public JAXBElement<UpdatePackageStatusByIdResponse> createUpdatePackageStatusByIdResponse(UpdatePackageStatusByIdResponse value) {
        return new JAXBElement<UpdatePackageStatusByIdResponse>(_UpdatePackageStatusByIdResponse_QNAME, UpdatePackageStatusByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePackageStatusById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "updatePackageStatusById")
    public JAXBElement<UpdatePackageStatusById> createUpdatePackageStatusById(UpdatePackageStatusById value) {
        return new JAXBElement<UpdatePackageStatusById>(_UpdatePackageStatusById_QNAME, UpdatePackageStatusById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchPackageById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "searchPackageById")
    public JAXBElement<SearchPackageById> createSearchPackageById(SearchPackageById value) {
        return new JAXBElement<SearchPackageById>(_SearchPackageById_QNAME, SearchPackageById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListAllPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "listAllPackageResponse")
    public JAXBElement<ListAllPackageResponse> createListAllPackageResponse(ListAllPackageResponse value) {
        return new JAXBElement<ListAllPackageResponse>(_ListAllPackageResponse_QNAME, ListAllPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListAllPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "listAllPackage")
    public JAXBElement<ListAllPackage> createListAllPackage(ListAllPackage value) {
        return new JAXBElement<ListAllPackage>(_ListAllPackage_QNAME, ListAllPackage.class, null, value);
    }

}
