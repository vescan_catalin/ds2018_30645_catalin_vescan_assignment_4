package com.client.controller;

import com.client.view.AdminView;
import com.client.ws.*;
import com.client.ws.Package;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminController {

    private AdminView adminView;

    private PackageWebService packageWebService;

    public AdminController() {}

    public AdminController(int id) {
        adminView = new AdminView();
        adminView.setVisible(true);

        packageWebService = new PackageWebServiceService().getPort(PackageWebService.class);

        adminView.addPackage(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String name = adminView.getName();
                String description = adminView.getDescription();
                String senderCity = adminView.getSenderCity();
                String destinationCity = adminView.getDestinationCity();
                Byte tracking = adminView.getTracking();
                int sender = adminView.getSender();
                int receiver = adminView.getReceiver();

                if(senderCity == null || destinationCity == null)
                    JOptionPane.showMessageDialog(null, "Error",
                            "Incomplete informations about package!", JOptionPane.ERROR_MESSAGE);

                PackageEntity packageEntity = new PackageEntity();
                packageEntity.setName(name);
                packageEntity.setDescription(description);
                packageEntity.setSenderCity(senderCity);
                packageEntity.setDestinationCity(destinationCity);
                packageEntity.setTracking(tracking);
                packageEntity.setSender(sender);
                packageEntity.setReceiver(receiver);

                packageWebService.addPackage(packageEntity);
            }
        });

        adminView.removePackage(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int packageId = adminView.getPackageId();

                packageWebService.removePackageById(packageId);
            }
        });

        adminView.registerPackage(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String name = adminView.getName();
                String description = adminView.getDescription();
                String senderCity = adminView.getSenderCity();
                String destinationCity = adminView.getDestinationCity();
                Byte tracking = adminView.getTracking();
                int sender = adminView.getSender();
                int receiver = adminView.getReceiver();
                int routeId = adminView.getRoute();

                PackageEntity packageEntity = new PackageEntity();
                packageEntity.setName(name);
                packageEntity.setDescription(description);
                packageEntity.setSenderCity(senderCity);
                packageEntity.setDestinationCity(destinationCity);
                packageEntity.setTracking(tracking);
                packageEntity.setSender(sender);
                packageEntity.setReceiver(receiver);
                packageEntity.setRoute(routeId);

                packageWebService.registerPackage(packageEntity);
            }
        });

        adminView.updatePackageStatus(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Byte tracking = adminView.getTracking();
                int packageId = adminView.getPackageId();

                String isTracking = (tracking == (byte)0)? "false" : "true";

                packageWebService.updatePackageStatusById(packageId, isTracking);
            }
        });
    }

}
