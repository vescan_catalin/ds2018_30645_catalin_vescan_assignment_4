package com.client.controller;

import com.client.view.UserView;
import com.client.ws.*;
import com.client.ws.Package;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class UserController {

    private UserView userView;

    private PackageWebService packageWebService;

    public UserController() {}

    public UserController(int id) {
        userView = new UserView();
        userView.setVisible(true);

        packageWebService = new PackageWebServiceService().getPort(PackageWebService.class);

        userView.listAllPackages(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                List<Package> packageEntityList = packageWebService.listAllPackage();

                for(Package each : packageEntityList) {
                    String pack = each.getId() + " " + each.getName() + " " + each.getRoute();
                    userView.print(pack);
                }
            }
        });

        userView.checkStatus(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int packageId = userView.getPackageId();

                String packageStatus = packageWebService.checkStatusByPackageId(packageId);

                userView.print(packageStatus);
            }
        });

        userView.searchPackage(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int packageId = userView.getPackageId();

                Package packageEntity = packageWebService.searchPackageById(packageId);

                userView.print(packageEntity.getId() + " " + packageEntity.getName());
            }
        });
    }
}
