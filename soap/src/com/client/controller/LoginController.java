package com.client.controller;

import com.client.view.LoginView;
import com.client.ws.PackageWebService;
import com.client.ws.PackageWebServiceService;
import com.client.ws.User;
import com.client.ws.UserEntity;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginController {

    private final String admin = "admin";
    private final String user = "user";

    private PackageWebService packageWebService;

    private LoginView login;

    public LoginController(){
        login = new LoginView();
        login.setVisible(true);

        packageWebService = new PackageWebServiceService().getPort(PackageWebService.class);

        login.login(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String username = login.getUsername();
                String password = login.getPassword();
                String type = null;

                if(!login.getAdminCheckBox().isSelected() && !login.getUserCheckBox().isSelected())
                    JOptionPane.showMessageDialog(null, "Error",
                            "Please select 1 option!", JOptionPane.ERROR_MESSAGE);
                if(login.getAdminCheckBox().isSelected() && login.getUserCheckBox().isSelected())
                    JOptionPane.showMessageDialog(null, "Error",
                            "Please select just 1 option!", JOptionPane.ERROR_MESSAGE);

                if(login.getAdminCheckBox().isSelected())
                    type = admin;
                else
                    type = user;

                User userEntity = packageWebService.getUserByUsername(username);

                if(userEntity == null || !userEntity.getPassword().equals(password))
                    JOptionPane.showMessageDialog(null, "Error",
                            "Wrong credentials!", JOptionPane.ERROR_MESSAGE);
                else {
                    if (userEntity.getType().equals(admin) && type.equals(admin))
                        new AdminController(userEntity.getId());
                    if (userEntity.getType().equals(user) && type.equals(user))
                        new UserController(userEntity.getId());
                }
            }
        });

        login.register(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String username = login.getUsername();
                String password = login.getPassword();
                String type = null;

                if(username.equals("") || password.equals(""))
                    JOptionPane.showMessageDialog(null, "Error",
                            "Fill all the fields for register!", JOptionPane.ERROR_MESSAGE);

                if(!login.getAdminCheckBox().isSelected() && !login.getUserCheckBox().isSelected())
                    JOptionPane.showMessageDialog(null, "Error",
                            "Please select 1 option!", JOptionPane.ERROR_MESSAGE);
                if(login.getAdminCheckBox().isSelected() && login.getUserCheckBox().isSelected())
                    JOptionPane.showMessageDialog(null, "Error",
                            "Please select just 1 option!", JOptionPane.ERROR_MESSAGE);

                if(login.getAdminCheckBox().isSelected())
                    type = "admin";
                else
                    type = "user";

                User userEntity = new User();
                userEntity.setUsername(username);
                userEntity.setPassword(password);
                userEntity.setType(type);

                packageWebService.saveNewUser(userEntity);
            }
        });
    }
}
