package com.client.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class AdminView extends JFrame {
    private JButton addPackageButton;
    private JButton registerPackageButton;
    private JButton packageStatusUpdatingButton;
    private JButton removePackageButton;
    private JTextField packageIdText;
    private JTextField nameText;
    private JTextField descriptionText;
    private JTextField senderCityText;
    private JTextField destinationCityText;
    private JTextField trackingText;
    private JTextField senderText;
    private JTextField receiverText;
    private JTextField routeText;

    public AdminView() {
        setTitle("Admin");
        setSize(900, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);

        JPanel panel = new JPanel();
        getContentPane().add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        JLabel title = new JLabel("Welcome!");
        title.setFont(new Font("Tahoma", Font.PLAIN, 45));
        title.setBounds(350, 50, 200, 50);
        panel.add(title);

        addPackageButton = new JButton("add package");
        addPackageButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
        addPackageButton.setBounds(50, 475, 150, 30);
        panel.add(addPackageButton);

        removePackageButton = new JButton("remove");
        removePackageButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
        removePackageButton.setBounds(260, 475, 150, 30);
        panel.add(removePackageButton);

        registerPackageButton = new JButton("register");
        registerPackageButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
        registerPackageButton.setBounds(480, 475, 150, 30);
        panel.add(registerPackageButton);

        packageStatusUpdatingButton = new JButton("update");
        packageStatusUpdatingButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
        packageStatusUpdatingButton.setBounds(700, 475, 150, 30);
        panel.add(packageStatusUpdatingButton);

        JLabel name = new JLabel("name");
        name.setFont(new Font("Tahoma", Font.PLAIN, 20));
        name.setBounds(50, 120, 150, 25);
        panel.add(name);

        JLabel description = new JLabel("description");
        description.setFont(new Font("Tahoma", Font.PLAIN, 20));
        description.setBounds(50, 160, 150, 25);
        panel.add(description);

        JLabel senderCity = new JLabel("sender city");
        senderCity.setFont(new Font("Tahoma", Font.PLAIN, 20));
        senderCity.setBounds(50, 200, 150, 25);
        panel.add(senderCity);

        JLabel destinationCity = new JLabel("destination city");
        destinationCity.setFont(new Font("Tahoma", Font.PLAIN, 20));
        destinationCity.setBounds(50, 240, 150, 25);
        panel.add(destinationCity);

        JLabel tracking = new JLabel("tracking");
        tracking.setFont(new Font("Tahoma", Font.PLAIN, 20));
        tracking.setBounds(50, 280, 150, 25);
        panel.add(tracking);

        JLabel sender = new JLabel("sender");
        sender.setFont(new Font("Tahoma", Font.PLAIN, 20));
        sender.setBounds(50, 320, 150, 25);
        panel.add(sender);

        JLabel receiver = new JLabel("receiver");
        receiver.setFont(new Font("Tahoma", Font.PLAIN, 20));
        receiver.setBounds(50, 360, 150, 25);
        panel.add(receiver);

        JLabel route = new JLabel("route");
        route.setFont(new Font("Tahoma", Font.PLAIN, 20));
        route.setBounds(50, 400, 150, 25);
        panel.add(route);

        JLabel packageId = new JLabel("packageId");
        packageId.setFont(new Font("Tahoma", Font.PLAIN, 20));
        packageId.setBounds(50, 440, 150, 25);
        panel.add(packageId);

        nameText = new JTextField();
        nameText.setFont(new Font("Tahoma", Font.PLAIN, 20));
        nameText.setBounds(260, 120, 200, 25);
        panel.add(nameText);
        nameText.setColumns(10);

        descriptionText = new JTextField();
        descriptionText.setFont(new Font("Tahoma", Font.PLAIN, 20));
        descriptionText.setColumns(10);
        descriptionText.setBounds(260, 160, 200, 25);
        panel.add(descriptionText);

        senderCityText = new JTextField();
        senderCityText.setFont(new Font("Tahoma", Font.PLAIN, 20));
        senderCityText.setColumns(10);
        senderCityText.setBounds(260, 200, 200, 25);
        panel.add(senderCityText);

        destinationCityText = new JTextField();
        destinationCityText.setFont(new Font("Tahoma", Font.PLAIN, 20));
        destinationCityText.setColumns(10);
        destinationCityText.setBounds(260, 240, 200, 25);
        panel.add(destinationCityText);

        trackingText = new JTextField();
        trackingText.setFont(new Font("Tahoma", Font.PLAIN, 20));
        trackingText.setColumns(10);
        trackingText.setBounds(260, 280, 200, 25);
        panel.add(trackingText);

        senderText = new JTextField();
        senderText.setFont(new Font("Tahoma", Font.PLAIN, 20));
        senderText.setColumns(10);
        senderText.setBounds(260, 320, 200, 25);
        panel.add(senderText);

        receiverText = new JTextField();
        receiverText.setFont(new Font("Tahoma", Font.PLAIN, 20));
        receiverText.setColumns(10);
        receiverText.setBounds(260, 360, 200, 25);
        panel.add(receiverText);

        routeText = new JTextField();
        routeText.setFont(new Font("Tahoma", Font.PLAIN, 20));
        routeText.setColumns(10);
        routeText.setBounds(260, 400, 200, 25);
        panel.add(routeText);

        packageIdText = new JTextField();
        packageIdText.setFont(new Font("Tahoma", Font.PLAIN, 20));
        packageIdText.setColumns(10);
        packageIdText.setBounds(260, 440, 200, 25);
        panel.add(packageIdText);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(550, 120, 275, 300);
        panel.add(scrollPane);

        JTextArea textArea = new JTextArea();
        scrollPane.setViewportView(textArea);
    }

    public void addPackage(ActionListener listener) {
        addPackageButton.addActionListener(listener);
    }

    public void removePackage(ActionListener listener) {
        removePackageButton.addActionListener(listener);
    }

    public void registerPackage(ActionListener listener) {
        registerPackageButton.addActionListener(listener);
    }

    public void updatePackageStatus(ActionListener listener) {
        packageStatusUpdatingButton.addActionListener(listener);
    }

    public String getName() {
        return nameText.getText();
    }

    public String getDescription() {
        return descriptionText.getText();
    }

    public String getSenderCity() {
        return senderCityText.getText();
    }

    public String getDestinationCity() {
        return destinationCityText.getText();
    }

    public Byte getTracking() {
        return (byte) (trackingText.getText().equals("true") ? 1 : 0);
    }

    public int getSender() {
        return Integer.parseInt(senderText.getText());
    }

    public int getReceiver() {
        return Integer.parseInt(receiverText.getText());
    }

    public int getRoute() {
        return Integer.parseInt(routeText.getText());
    }

    public int getPackageId() {
        return  Integer.parseInt(packageIdText.getText());
    }
}
