package com.client.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JButton;

public class LoginView extends JFrame {
    private final JButton loginButton;
    private final JRadioButton adminBox;
    private final JRadioButton userBox;
    private final JButton registerButton;
    private JTextField usernameText;
    private JTextField passwordText;
    public LoginView() {
        setTitle("SOAP");
        setSize(900, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);

        JPanel panel = new JPanel();
        getContentPane().add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        JLabel title = new JLabel("Welcome!");
        title.setFont(new Font("Tahoma", Font.PLAIN, 45));
        title.setBounds(350, 50, 200, 50);
        panel.add(title);

        JLabel username = new JLabel("username");
        username.setFont(new Font("Tahoma", Font.PLAIN, 25));
        username.setBounds(200, 200, 150, 30);
        panel.add(username);

        JLabel password = new JLabel("password");
        password.setFont(new Font("Tahoma", Font.PLAIN, 25));
        password.setBounds(200, 300, 150, 30);
        panel.add(password);

        usernameText = new JTextField();
        usernameText.setFont(new Font("Tahoma", Font.PLAIN, 25));
        usernameText.setColumns(10);
        usernameText.setBounds(350, 200, 300, 30);
        panel.add(usernameText);

        passwordText = new JTextField();
        passwordText.setFont(new Font("Tahoma", Font.PLAIN, 25));
        passwordText.setColumns(10);
        passwordText.setBounds(350, 300, 300, 30);
        panel.add(passwordText);

        adminBox = new JRadioButton("Admin");
        adminBox.setFont(new Font("Tahoma", Font.PLAIN, 25));
        adminBox.setBounds(200, 380, 150, 30);
        panel.add(adminBox);

        userBox = new JRadioButton("User");
        userBox.setFont(new Font("Tahoma", Font.PLAIN, 25));
        userBox.setBounds(500, 380, 150, 30);
        panel.add(userBox);

        loginButton = new JButton("Login");
        loginButton.setFont(new Font("Tahoma", Font.PLAIN, 35));
        loginButton.setBounds(450, 450, 200, 50);
        panel.add(loginButton);

        registerButton = new JButton("Register");
        registerButton.setFont(new Font("Tahoma", Font.PLAIN, 35));
        registerButton.setBounds(200, 450, 200, 50);
        //panel.add(registerButton);
    }

    public void login(ActionListener listener) {
        loginButton.addActionListener(listener);
    }

    public void register(ActionListener listener) {
        registerButton.addActionListener(listener);
    }

    public String getUsername() {
        return usernameText.getText();
    }

    public String getPassword() {
        return passwordText.getText();
    }

    public JRadioButton getAdminCheckBox() {
        return adminBox;
    }

    public JRadioButton getUserCheckBox() {
        return userBox;
    }

}
