package com.client.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;

public class UserView extends JFrame {
    private final JButton searchPackage;
    private final JButton listAllPackages;
    private final JButton statusChecking;
    private final JTextArea textArea;
    private JTextField packageIdText;
    public UserView() {
        setTitle("Regular User");
        setSize(900, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);

        JPanel panel = new JPanel();
        getContentPane().add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        JLabel title = new JLabel("Welcome!");
        title.setFont(new Font("Tahoma", Font.PLAIN, 45));
        title.setBounds(350, 50, 200, 50);
        panel.add(title);

        textArea = new JTextArea();
        textArea.setFont(new Font("Monospaced", Font.PLAIN, 16));
        textArea.setBounds(550, 125, 300, 375);
        textArea.setEditable(false);
        panel.add(textArea);

        searchPackage = new JButton("search package");
        searchPackage.setFont(new Font("Tahoma", Font.PLAIN, 25));
        searchPackage.setBounds(300, 300, 225, 30);
        panel.add(searchPackage);

        listAllPackages = new JButton("list all");
        listAllPackages.setFont(new Font("Tahoma", Font.PLAIN, 25));
        listAllPackages.setBounds(300, 150, 225, 30);
        panel.add(listAllPackages);

        statusChecking = new JButton("status checking");
        statusChecking.setFont(new Font("Tahoma", Font.PLAIN, 25));
        statusChecking.setBounds(300, 450, 225, 30);
        panel.add(statusChecking);

        JLabel packageId = new JLabel("package id");
        packageId.setFont(new Font("Tahoma", Font.PLAIN, 20));
        packageId.setBounds(30, 260, 100, 30);
        panel.add(packageId);

        packageIdText = new JTextField();
        packageIdText.setBounds(30, 300, 100, 30);
        panel.add(packageIdText);
        packageIdText.setColumns(10);
    }

    public void listAllPackages(ActionListener listener) {
        listAllPackages.addActionListener(listener);
    }

    public void searchPackage(ActionListener listener) {
        searchPackage.addActionListener(listener);
    }

    public void checkStatus(ActionListener listener) {
        statusChecking.addActionListener(listener);
    }

    public int getPackageId() {
        return Integer.parseInt(packageIdText.getText());
    }
    
    public void print(String text) {
        textArea.append(text);
        textArea.append("\n");
    }
}

