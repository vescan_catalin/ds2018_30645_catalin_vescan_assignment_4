package entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "package", schema = "soap")
public class PackageEntity implements Serializable {
    private int id;
    private String name;
    private String description;
    private String senderCity;
    private String destinationCity;
    private Byte tracking;
    private Integer sender;
    private Integer receiver;
    private Integer route;
    private UserEntity userBySender;
    private UserEntity userByReceiver;
    private RouteEntity routeByRoute;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "senderCity")
    public String getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(String senderCity) {
        this.senderCity = senderCity;
    }

    @Basic
    @Column(name = "destinationCity")
    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    @Basic
    @Column(name = "tracking")
    public Byte getTracking() {
        return tracking;
    }

    public void setTracking(Byte tracking) {
        this.tracking = tracking;
    }

    @Basic
    @Column(name = "sender")
    public Integer getSender() {
        return sender;
    }

    public void setSender(Integer sender) {
        this.sender = sender;
    }

    @Basic
    @Column(name = "receiver")
    public Integer getReceiver() {
        return receiver;
    }

    public void setReceiver(Integer receiver) {
        this.receiver = receiver;
    }

    @Basic
    @Column(name = "route")
    public Integer getRoute() {
        return route;
    }

    public void setRoute(Integer route) {
        this.route = route;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PackageEntity that = (PackageEntity) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (senderCity != null ? !senderCity.equals(that.senderCity) : that.senderCity != null) return false;
        if (destinationCity != null ? !destinationCity.equals(that.destinationCity) : that.destinationCity != null)
            return false;
        if (tracking != null ? !tracking.equals(that.tracking) : that.tracking != null) return false;
        if (sender != null ? !sender.equals(that.sender) : that.sender != null) return false;
        if (receiver != null ? !receiver.equals(that.receiver) : that.receiver != null) return false;
        return route != null ? route.equals(that.route) : that.route == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (senderCity != null ? senderCity.hashCode() : 0);
        result = 31 * result + (destinationCity != null ? destinationCity.hashCode() : 0);
        result = 31 * result + (tracking != null ? tracking.hashCode() : 0);
        result = 31 * result + (sender != null ? sender.hashCode() : 0);
        result = 31 * result + (receiver != null ? receiver.hashCode() : 0);
        result = 31 * result + (route != null ? route.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "sender", referencedColumnName = "id", insertable = false, updatable = false)
    public UserEntity getUserBySender() {
        return userBySender;
    }

    public void setUserBySender(UserEntity userBySender) {
        this.userBySender = userBySender;
    }

    @ManyToOne
    @JoinColumn(name = "receiver", referencedColumnName = "id", insertable = false, updatable = false)
    public UserEntity getUserByReceiver() {
        return userByReceiver;
    }

    public void setUserByReceiver(UserEntity userByReceiver) {
        this.userByReceiver = userByReceiver;
    }

    @ManyToOne
    @JoinColumn(name = "route", referencedColumnName = "id", insertable = false, updatable = false)
    public RouteEntity getRouteByRoute() {
        return routeByRoute;
    }

    public void setRouteByRoute(RouteEntity routeByRoute) {
        this.routeByRoute = routeByRoute;
    }

    @Override
    public String toString() {
        return "PackageEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", senderCity='" + senderCity + '\'' +
                ", destinationCity='" + destinationCity + '\'' +
                ", tracking=" + tracking +
                ", sender=" + sender +
                ", receiver=" + receiver +
                ", route=" + route +
                ", userBySender=" + userBySender +
                ", userByReceiver=" + userByReceiver +
                ", routeByRoute=" + routeByRoute +
                '}';
    }
}
