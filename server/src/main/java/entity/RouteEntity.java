package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;

@Entity
@Table(name = "route", schema = "soap")
public class RouteEntity implements Serializable {
    private int id;
    private String city;
    private Date time;
    private Collection<PackageEntity> packagesById;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "time")
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RouteEntity that = (RouteEntity) o;

        if (id != that.id) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        return time != null ? time.equals(that.time) : that.time == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "routeByRoute")
    public Collection<PackageEntity> getPackagesById() {
        return packagesById;
    }

    public void setPackagesById(Collection<PackageEntity> packagesById) {
        this.packagesById = packagesById;
    }

    @Override
    public String toString() {
        return "RouteEntity{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", time=" + time +
                ", packagesById=" + packagesById +
                '}';
    }
}
