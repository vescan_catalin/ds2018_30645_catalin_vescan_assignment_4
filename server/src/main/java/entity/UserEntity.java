package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name = "user", schema = "soap")
public class UserEntity implements Serializable {
    private int id;
    private String username;
    private String password;
    private String type;
    private Collection<PackageEntity> packagesById;
    private Collection<PackageEntity> packagesById_0;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEntity that = (UserEntity) o;

        if (id != that.id) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        return type != null ? type.equals(that.type) : that.type == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "userBySender")
    public Collection<PackageEntity> getPackagesById() {
        return packagesById;
    }

    public void setPackagesById(Collection<PackageEntity> packagesById) {
        this.packagesById = packagesById;
    }

    @OneToMany(mappedBy = "userByReceiver")
    public Collection<PackageEntity> getPackagesById_0() {
        return packagesById_0;
    }

    public void setPackagesById_0(Collection<PackageEntity> packagesById_0) {
        this.packagesById_0 = packagesById_0;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", type='" + type + '\'' +
                ", packagesById=" + packagesById +
                ", packagesById_0=" + packagesById_0 +
                '}';
    }
}
