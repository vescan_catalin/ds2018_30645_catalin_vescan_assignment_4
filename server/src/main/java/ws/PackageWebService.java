package ws;

import dto.Package;
import dto.Route;
import dto.User;
import entity.PackageEntity;
import entity.RouteEntity;
import entity.UserEntity;
import implementation.PackageService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@SuppressWarnings("ALL")
@WebService
public class PackageWebService {

    public PackageWebService() {

    }

    @WebMethod
    public User getUserByUsername(String username) {
        PackageService packageService = new PackageService();
        return packageService.getUserByUsername(username);
    }

    @WebMethod
    public void saveNewUser(User newUser) {
        PackageService packageService = new PackageService();
        packageService.saveNewUser(newUser);
    }

    @WebMethod
    public void addPackage(PackageEntity pack) {
        PackageService packageService = new PackageService();
        packageService.addPackage(pack);
    }

    @WebMethod
    public void removePackageById(int id) {
        PackageService packageService = new PackageService();
        packageService.removePackageById(id);
    }

    @WebMethod
    public void registerPackage(PackageEntity pack) {
        PackageService packageService = new PackageService();
        packageService.registerPackage(pack);
    }

    @WebMethod
    public void updatePackageStatusById(int id, String isTracking) {
        PackageService packageService = new PackageService();
        packageService.updatePackageStatusById(id, isTracking);
    }

    @WebMethod
    public List<Package> listAllPackage() {
        PackageService packageService = new PackageService();
        return packageService.listAllPackage();
    }

    @WebMethod
    public Package searchPackageById(int id) {
        PackageService packageService = new PackageService();
        return packageService.searchPackageById(id);
    }

    @WebMethod
    public String checkStatusByPackageId(int id) {
        PackageService packageService = new PackageService();
        return packageService.checkStatusByPackageId(id);
    }

    @WebMethod
    public Route getRouteById(int id) {
        PackageService packageService = new PackageService();
        return  packageService.getRouteById(id);
    }

    @WebMethod
    public Route getPackageRoute(int routeByPackageId) {
        PackageService packageService = new PackageService();
        return packageService.getPackageRoute(routeByPackageId);
    }
}
