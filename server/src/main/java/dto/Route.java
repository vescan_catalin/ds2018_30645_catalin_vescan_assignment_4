package dto;

import entity.PackageEntity;
import entity.RouteEntity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;

public class Route implements Serializable {
    private int id;
    private String city;
    private Date time;
    private Collection<PackageEntity> packagesById;

    public Route() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Collection<PackageEntity> getPackagesById() {
        return packagesById;
    }

    public void setPackagesById(Collection<PackageEntity> packagesById) {
        this.packagesById = packagesById;
    }

    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", time=" + time +
                '}';
    }
}
