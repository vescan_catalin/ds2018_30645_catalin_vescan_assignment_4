package dto;

import entity.PackageEntity;
import entity.UserEntity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Collection;

public class User implements Serializable {
    private int id;
    private String username;
    private String password;
    private String type;
    private Collection<PackageEntity> packagesById;
    private Collection<PackageEntity> packagesById_0;

    public User() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Collection<PackageEntity> getPackagesById() {
        return packagesById;
    }

    public void setPackagesById(Collection<PackageEntity> packagesById) {
        this.packagesById = packagesById;
    }

    public Collection<PackageEntity> getPackagesById_0() {
        return packagesById_0;
    }

    public void setPackagesById_0(Collection<PackageEntity> packagesById_0) {
        this.packagesById_0 = packagesById_0;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
