package dto;

import entity.RouteEntity;
import entity.UserEntity;

import java.io.Serializable;

public class Package implements Serializable {
    private int id;
    private String name;
    private String description;
    private String senderCity;
    private String destinationCity;
    private Byte tracking;
    private Integer sender;
    private Integer receiver;
    private Integer route;
    private UserEntity userBySender;
    private UserEntity userByReceiver;
    private RouteEntity routeByRoute;

    public Package() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(String senderCity) {
        this.senderCity = senderCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public Byte getTracking() {
        return tracking;
    }

    public void setTracking(Byte tracking) {
        this.tracking = tracking;
    }

    public Integer getSender() {
        return sender;
    }

    public void setSender(Integer sender) {
        this.sender = sender;
    }

    public Integer getReceiver() {
        return receiver;
    }

    public void setReceiver(Integer receiver) {
        this.receiver = receiver;
    }

    public Integer getRoute() {
        return route;
    }

    public void setRoute(Integer route) {
        this.route = route;
    }

    public UserEntity getUserBySender() {
        return userBySender;
    }

    public void setUserBySender(UserEntity userBySender) {
        this.userBySender = userBySender;
    }

    public UserEntity getUserByReceiver() {
        return userByReceiver;
    }

    public void setUserByReceiver(UserEntity userByReceiver) {
        this.userByReceiver = userByReceiver;
    }

    public RouteEntity getRouteByRoute() {
        return routeByRoute;
    }

    public void setRouteByRoute(RouteEntity routeByRoute) {
        this.routeByRoute = routeByRoute;
    }

    @Override
    public String toString() {
        return "Package{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", senderCity='" + senderCity + '\'' +
                ", destinationCity='" + destinationCity + '\'' +
                ", tracking=" + tracking +
                ", sender=" + sender +
                ", receiver=" + receiver +
                ", route=" + route +
                '}';
    }
}
