package implementation;


import database.Connection;
import dto.Package;
import dto.Route;
import dto.User;
import entity.PackageEntity;
import entity.RouteEntity;
import entity.UserEntity;
import interfaces.Communication;
import org.hibernate.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
@WebService
public class PackageService implements Communication {

    private Connection connection;
    private Session session;

    public PackageService() {
        this.connection = new Connection();
        this.session = connection.getSession();
    }

    @Override
    @WebMethod
    public User getUserByUsername(String username) {
        if(session != null) {
            UserEntity userEntity = session.createNativeQuery("select * from user where username = (:username)", UserEntity.class)
                    .setParameter("username", username)
                    .getResultList()
                    .get(0);

            User user = new User();
            user.setId(userEntity.getId());
            user.setUsername(userEntity.getUsername());
            user.setPassword(userEntity.getPassword());
            user.setType(userEntity.getType());

            return user;
        }

        return null;
    }

    @Override
    @WebMethod
    public void saveNewUser(User newUser) {
        if(session != null) {
            session.save(newUser);
            session.getTransaction().commit();
        }
    }

    @Override
    @WebMethod
    public void addPackage(PackageEntity pack) {
        if (session != null) {
            session.save(pack);
            session.getTransaction().commit();
        }
    }

    @Override
    @WebMethod
    public void removePackageById(int id) {
        if(session != null) {
            session.createNativeQuery("delete from package where id = " + id)
                    .executeUpdate();
            session.getTransaction().commit();
        }
    }

    @Override
    @WebMethod
    public void registerPackage(PackageEntity pack) {
        if(session != null) {
            pack.setTracking((byte) 1);
            pack.setRoute(pack.getRoute());

            session.save(pack);
            session.getTransaction().commit();
        }
    }

    @Override
    @WebMethod
    public void updatePackageStatusById(int id, String isTracking) {
        Byte isTrackingTrue;

        if (session != null)
            if (isTracking.equals("true")) {
                isTrackingTrue = 1;
                session.createNativeQuery("update package set tracking = " + isTrackingTrue + " where id = (:id)")
                        .setParameter("id", id)
                        .executeUpdate();
            }
            else {
                isTrackingTrue = 0;
                session.createNativeQuery("update package set tracking = " + isTrackingTrue + " where id = (:id)")
                        .setParameter("id", id)
                        .executeUpdate();
            }

        session.getTransaction().commit();
    }

    @Override
    @WebMethod
    public List<Package> listAllPackage() {
        if(session != null) {
            List<PackageEntity> packageEntities = session.createNativeQuery("select * from package", PackageEntity.class).list();

            return packageEntities.stream()
                    .map(entity -> {
                        Package p = new Package();
                        p.setId(entity.getId());
                        p.setName(entity.getName());
                        p.setDescription(entity.getDescription());
                        p.setSenderCity(entity.getSenderCity());
                        p.setDestinationCity(entity.getDestinationCity());
                        p.setTracking(entity.getTracking());
                        p.setSender(entity.getSender());
                        p.setReceiver(entity.getReceiver());
                        p.setRoute(entity.getRoute());

                        return p;
                    }).collect(Collectors.toList());

        }

        return null;
    }

    @Override
    @WebMethod
    public Package searchPackageById(int id) {
        if(session != null) {
            PackageEntity packageEntity = session.createNativeQuery(
                    "select * from package where id = (:id)", PackageEntity.class)
                    .setParameter("id", id)
                    .getResultList()
                    .get(0);

            Package pack = new Package();
            pack.setId(packageEntity.getId());
            pack.setName(packageEntity.getName());
            pack.setDescription(packageEntity.getDescription());
            pack.setSenderCity(packageEntity.getSenderCity());
            pack.setDestinationCity(packageEntity.getDestinationCity());
            pack.setTracking(packageEntity.getTracking());
            pack.setSender(packageEntity.getSender());
            pack.setReceiver(packageEntity.getReceiver());
            pack.setRoute(packageEntity.getRoute());

            return pack;
        }

        return null;
    }

    @Override
    @WebMethod
    public String checkStatusByPackageId(int id) {
        if(session != null) {
            PackageEntity packageEntity = (PackageEntity) session.createNativeQuery(
                    "select * from package where id = (:id)", PackageEntity.class)
                    .setParameter("id", id).getResultList().get(0);

            if(packageEntity.getTracking() == 0)
                return "This package is not tracking.";
            else
                return "This package is tracking.";
        }

        return null;
    }

    @Override
    public Route getRouteById(int id) {
        if(session != null) {
            RouteEntity routeEntity = session.createNativeQuery(
                    "select * from route where id = (:id)", RouteEntity.class)
                    .setParameter("id", id)
                    .getResultList()
                    .get(0);

            Route route = new Route();
            route.setId(routeEntity.getId());
            route.setCity(routeEntity.getCity());
            route.setTime(routeEntity.getTime());

            return route;
        }

        return null;
    }

    @Override
    public Route getPackageRoute(int routeByPackageId) {
        if(session != null) {
            /*String packageRouteId = "select route from package where id = ";
            String getRouteWithId = "select * from route where id =";

            RouteEntity routeEntity = session.createNativeQuery(
                    getRouteWithId + packageRouteId + routeByPackageId, RouteEntity.class)
                    .getResultList()
                    .get(0); */

            PackageEntity packageEntity = (PackageEntity) session.createNativeQuery(
                    "select * from package where id = (:id)", PackageEntity.class)
                    .setParameter("id", routeByPackageId)
                    .getResultList()
                    .get(0);

            RouteEntity routeEntity = session.createNativeQuery(
                    "select * from route where id = (:id)", RouteEntity.class)
                    .setParameter("id", packageEntity.getRoute())
                    .getResultList()
                    .get(0);

            Route route = new Route();
            route.setId(routeEntity.getId());
            route.setCity(routeEntity.getCity());
            route.setTime(routeEntity.getTime());

            return route;
        }

        return null;
    }
}
