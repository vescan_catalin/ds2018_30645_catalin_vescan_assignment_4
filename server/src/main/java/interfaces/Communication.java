package interfaces;

import dto.Package;
import dto.Route;
import dto.User;
import entity.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface Communication {

    // login and register
    @WebMethod
    User getUserByUsername(String username);

    @WebMethod
    void saveNewUser(User newUser);

    // operations can do by admin
    @WebMethod
    void addPackage(PackageEntity pack);

    @WebMethod
    void removePackageById(int id);

    @WebMethod
    void registerPackage(PackageEntity pack);

    @WebMethod
    void updatePackageStatusById(int id, String isTracking);

    // operations can do by regular user
    @WebMethod
    List<Package> listAllPackage();

    @WebMethod
    Package searchPackageById(int id);

    @WebMethod
    String checkStatusByPackageId(int id);

    @WebMethod
    Route getRouteById(int id);

    @WebMethod
    Route getPackageRoute(int routeByPackageId);
}
